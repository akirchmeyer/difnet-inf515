import jax.numpy as jnp
import numpy as np
import json

import mesh_to_sdf
from sklearn.neighbors import KDTree
from sklearn.neighbors import NearestNeighbors
import skimage.measure

import trimesh 
#import mesh_to_sdf
#import pymeshlab
import matplotlib

import os

def chamfery2x(x, y):
    y_nn = NearestNeighbors(n_neighbors=1, leaf_size=1, algorithm='kd_tree', metric='l2').fit(y)
    min_x_to_y = y_nn.kneighbors(x)[0]
    chamfer_dist = np.mean(min_x_to_y)
    return chamfer_dist

#adapted from difnet
def generateMeshFromSDF(model, N=64, max_batch = 2**18, origin = np.zeros(3)):
    # NOTE: the voxel_origin is actually the (bottom, left, down) corner, not the middle
    voxel_origin = -np.ones(3)
    voxel_size = 2.0 / (N - 1)
    overall_index = np.arange(0, N ** 3, 1).astype(np.long)
    samples = np.zeros((N ** 3, 3))
    samples[:, 2] = overall_index % N
    samples[:, 1] = (overall_index // N) % N
    samples[:, 0] = ((overall_index // N) // N) % N
    samples = (samples*voxel_size + voxel_origin).astype(np.float)
     
    num_samples = N ** 3

    head = 0
    
    sdf_values = np.zeros(N**3)
    for head in range(0, num_samples, max_batch):
        sample_subset = samples[head : min(head + max_batch, num_samples)]
        sdf_values[head : min(head + max_batch, num_samples)] = model(sample_subset).flatten()        
    #return samples, sdf_values
    sdf_values = sdf_values.reshape(N, N, N)

    try: 
        verts, faces, normals, values = skimage.measure.marching_cubes(
            sdf_values, level=0.0, spacing=[voxel_size] * 3
        )

        # transform from voxel coordinates to camera coordinates
        # note x and y are flipped in the output of marching_cubes
        mesh_points = verts + voxel_origin + origin

        return trimesh.Trimesh(vertices=mesh_points, faces=faces)
    except ValueError:
        return trimesh.Trimesh()

# adapted from mesh_to_sdf
# def generate_samples_queries(mesh, nu, nns):
#     uniform = np.random.uniform(-1, 1, (nu, 3))
#     on_surface, idxs = trimesh.sample.sample_surface(mesh, nns)
#     on_surface_normals = mesh.face_normals[idxs, :]
#     on_surface1, _ = trimesh.sample.sample_surface(mesh, nns//2)
#     on_surface2, _ = trimesh.sample.sample_surface(mesh, nns//2)
    
#     near_surface1 = on_surface1 + np.random.normal(scale=0.0025, size=(nns//2, 3))
#     near_surface2 = on_surface2 + np.random.normal(scale=0.00025, size=(nns//2, 3))
#     off_surface = np.random.permutation(np.vstack([uniform, near_surface1, near_surface2]))
#     m = nu//2 + nns//2
#     return on_surface,on_surface_normals,off_surface[:m], off_surface[m:]

def generate_samples_queries(mesh, nu, nns):
    uniform = np.random.uniform(-1, 1, (nu, 3))
    #indices = points[np.random.choice(points.shape[0], nns), :]
    #on_surface = points[indices, :]
    on_surface, _ = trimesh.sample.sample_surface(mesh, nns)
    
    near_surface1 = on_surface + np.random.normal(scale=0.0025, size=(nns, 3))
    near_surface2 = on_surface + np.random.normal(scale=0.00025, size=(nns, 3))
    return np.vstack([uniform, near_surface1, near_surface2])

# adapted from mesh_to_sdf
def scale_to_unit_sphere(mesh, scale=0.9):
    if isinstance(mesh, trimesh.Scene):
        mesh = mesh.dump().sum()
    scale /= np.max(np.linalg.norm(mesh.vertices, axis=1))
    matrix = np.eye(4)
    matrix[:3, :3] *= scale
    matrix[3, :3] = -mesh.bounding_box.centroid
    mesh.apply_transform(matrix)

# def generate_samples_real_sdf(mesh, n_samples, nu = 1000, k = 5):
#     assert(n_samples >= 2*nu)
#     nns = (n_samples - nu)//2
#     on_surface, on_surface_normals, off_surface1, off_surface2 = generate_samples_queries(mesh, nu, nns)
#     #surface_point_cloud = mesh_to_sdf.get_surface_point_cloud(mesh, 'scan', 1, scan_count=100, scan_resolution=400, sample_point_count=10000000, calculate_normals=True)
#     queries = np.vstack([off_surface1, off_surface2])
#     #sdfs, gradients = surface_point_cloud.get_sdf_in_batches(queries, use_depth_buffer=False, sample_count=k, return_gradients=True)
#     closest,distance,_ = trimesh.proximity.closest_point(mesh, queries, )
#     gradients = queries - closest
#     gradients /= np.linalg.norm(gradients, axis=-1, keepdims=True)
#     sdfs = -trimesh.proximity.signed_distance(mesh, queries)
#     #signs = trimesh.ray.ray_pyembree.RayMeshIntersector(mesh, queries)
#     #sdfs = distance#np.where(signs, -1, 1) * distance

#     queries = np.vstack([on_surface, queries])
#     #sdfs = np.hstack([np.zeros(len(on_surface)), sdfs[:len(off_surface1)], -np.ones(len(off_surface2))])
#     sdfs = np.hstack([np.zeros(len(on_surface)), sdfs])
#     gradients = np.vstack([-on_surface_normals, gradients])
#     return np.hstack([queries, sdfs.reshape((-1,1)), gradients])

def generate_samples_real_sdf(mesh, n_samples, nu = 1000, k = 5):
    assert(n_samples >= 3*nu)
    nos = (n_samples - nu)//2
    queries = generate_samples_queries(mesh, nu, nos)
    #surface_point_cloud = mesh_to_sdf.get_surface_point_cloud(mesh, 'scan', 1, scan_count=100, scan_resolution=400, sample_point_count=10000000, calculate_normals=True)
    surface_point_cloud = mesh_to_sdf.get_surface_point_cloud(mesh, 'scan', None, scan_count=100, scan_resolution=400, sample_point_count=1000000, calculate_normals=True)
    sdfs, gradients = surface_point_cloud.get_sdf_in_batches(queries, use_depth_buffer=True, sample_count=9, return_gradients=True)
    #sdfs, gradients = generate_samples_sdfs(points, normals, queries, **kwargs)
    return np.hstack([queries, sdfs.reshape((-1,1)), gradients])
    #points, sdf, normals = mesh_to_sdf.sample_sdf_near_surface(mesh, number_of_points=n_samples, return_gradients=True, normal_sample_count=11)
    #return np.hstack([points, sdf.reshape((-1,1)), normals])

def generate_sample_file(baseFile, n_samples, scale=1, expand=0.0, saveFile=''):
    ms = trimesh.load_mesh(f'{baseFile}.obj')
    if isinstance(ms, trimesh.Scene):
        ms = ms.dump(concatenate=True)
    ms = trimesh.Trimesh(vertices=ms.vertices+expand*ms.vertex_normals, faces=ms.faces, vertex_normals=ms.vertex_normals) 
    scale_to_unit_sphere(ms, scale=scale)
    point_cloud = generate_samples_real_sdf(ms, n_samples = n_samples)
    saveFile = baseFile if saveFile == '' else saveFile
    np.save(open(f'{saveFile}.xyzs', 'wb'), point_cloud)

def scale_array(v, m = None, M = None):
    m = np.min(v) if m is None else m
    M = np.max(v) if M is None else M
    return (np.minimum(v,M)-m)/(M-m)

def trimesh_colored_pc(point, sdfs, origin=0, eps=0.1, method='posneg', m=None, M=None):
    if method == 'posneg':
        return trimesh.points.PointCloud(point+origin, colors=colormapHuePositiveNegative(sdfs))
    if method == 'scaled':
        return trimesh.points.PointCloud(point+origin, colors=colormapHueScaled(sdfs, m, M))

def trimesh_colored_normals(points, normals, origin=0):
    colors = scale_array(normals, -1, 1)
    return trimesh.points.PointCloud(points+origin, colors=colors)
    

def colormapHueScaled(values, m, M):
    m = np.min(values) if m is None else m
    M = np.max(values) if M is None else M
    scaled_values = (values.astype(np.float)-m)/(M-m)
    # hsv: red to blue
    colors = 255*matplotlib.colors.hsv_to_rgb([[x/1.5, 1.0, 1.0] for x in scaled_values])
    return colors

def colormapHuePositive(values):
    scale = max(np.max(values),0)
    colors = 255*matplotlib.colors.hsv_to_rgb([[max(x/scale,0)/1.5, 1.0, float(x >= 0)] for x in values])
    return colors

def colormapHuePositiveNegative(values):
    scale = 3.0*np.array([max(np.max(values),0), max(np.max(-values),0)])
    orig = np.array([0,0.75])
    isneg = np.array(values < 0, dtype=np.int)
    colors = 255*matplotlib.colors.hsv_to_rgb([[orig[isneg[k]] + x/scale[isneg[k]], 1.0, 1.0] for k,x in enumerate(values)])
    return colors


def cos_similarity(xs, ns):
    return jnp.sum(xs*ns, axis=-1)/(jnp.linalg.norm(xs, axis=-1))#*jnp.linalg.norm(ns, axis=-1))

def load_samples(baseFile, n_samples, force = False):
    if force or not os.path.exists(f'{baseFile}.2.xyzs'):
        baseFile = f'{baseFile}'
        generate_sample_file(baseFile, n_samples, saveFile=f'{baseFile}.2')
        print(f'Created pointcloud .xyzs file')
    else:
        print('Reusing existing .xyzs file')
    samples = np.load(f'{baseFile}.2.xyzs')
    n_samples = len(samples)
    print(f'Loaded {baseFile}.2.xyzs with n_samples={n_samples}')
    return samples, n_samples

def read_keypoints(baseFile, model_ids, n_kpc):
    data = json.load(open(baseFile, 'rb'))
    res = np.zeros((len(model_ids), n_kpc, 3))
    for mesh_keypoint in data:
        model_id = mesh_keypoint['model_id']
        if not model_id in model_ids:
            continue
        res_id = model_ids[model_id]
        for model_kp in mesh_keypoint['keypoints']:
            res[res_id, model_kp['semantic_id'], :] = np.array(model_kp['xyz'])
        print(f'done with {res_id} - {model_id}')
    return res

def read_models_with_keypoints(baseFile):
    n_kpc = 0
    data = json.load(open(baseFile, 'rb'))
    res = []
    for mesh_keypoint in data:
        model_id = mesh_keypoint['model_id']
        for model_kp in mesh_keypoint['keypoints']:
            n_kpc = max(n_kpc, model_kp['semantic_id']+1)
        res.append(model_id)
    return res, n_kpc
        
