# Modules
import time, os
import random
import numpy as np
import jax
import jax.numpy as jnp
from jax.experimental import optimizers, stax
import pickle
import matplotlib.pyplot as plt
import trimesh
from sklearn.neighbors import KDTree

from utils import *
from models import *
from config import annotationsFile, baseDirectory, modelsDirectory, visualizationDirectory

# Colored output on terminal
import sys
from IPython.core import ultratb
sys.excepthook = ultratb.FormattedTB(color_scheme='Linux', call_pdb=False)

checkpointFile = f'{baseDirectory}/checkpoints/best.cars.dif.ckpt.pkl.1.800'

# Constants

n_samples = 2**14#2 ** 15
n_models = 2#5
learning_rate = 1e-4
num_epochs = 1000
hidf = 128
n_hidden = 2
latent_dim = 64#128
hhidf = 256#256
hn_hidden = 1



flagVisualize = True
flagLoad = True # True
flagSave = False
flagTrain = False


# Create network
seed = 0
random.seed(0)
key = jax.random.PRNGKey(seed)
key, key1, key2, key3,key4 = jax.random.split(key, num=5)

template_params, template_apply = nn_instance(key1, inf=3, n_samples=n_samples, net=nn_bvp(inf=3, hidf=hidf, outf=1, n_hidden=n_hidden))
deform_params, deform_apply = nn_instance(key2, inf=3, n_samples=1, net=nn_bvp(inf=3, hidf=hidf, outf=4, n_hidden=n_hidden, type='relu'))
print('deform-net shape:', net_shape(deform_params))
hyper_params, hyper_apply = nn_instance(key3, inf=latent_dim, n_samples=n_samples, net=nn_hyper(inf=latent_dim, hidf=hhidf, n_hidden=hn_hidden, params=deform_params))

print('hyper-net shape:', net_shape(hyper_params))

latent_codes = jax.nn.initializers.normal(stddev=0.01)(key4, shape=(n_models, latent_dim))

# Load model
if flagLoad:
    hyper_params, template_params = pickle.load(open(checkpointFile, "rb"))
    print(f'Loaded model params from {checkpointFile}')

(template_opt_init, template_opt_update, template_opt_params) = optimizers.adam(step_size=learning_rate)
(hyper_opt_init, hyper_opt_update, hyper_opt_params) = optimizers.adam(step_size=learning_rate)
hyper_opt_state = hyper_opt_init(hyper_params)
template_opt_state = template_opt_init(template_params)

def nn_compute_vars(hypo_params,template_params,x):
    delta = deform_apply(hypo_params, x).squeeze()
    deformation = delta[:3]
    correction = delta[3]
    z = x + deformation
    sdf = template_apply(template_params, z)
    sdf_final = sdf + correction
    return z, deformation, correction, sdf.squeeze(), sdf_final.squeeze()

def batched_nn_compute_all(hyper_params, template_params, idxs, xs, calc_grad = False):
    embeddings = latent_codes[idxs, :] 
    hypo_params = hyper_apply(hyper_params, embeddings)
    zs, deformations, corrections, sdfs, sdfs_final = jax.vmap(nn_compute_vars, in_axes=(0,None,0))(hypo_params, template_params, xs)
    return embeddings, hypo_params, zs, deformations, corrections, sdfs, sdfs_final

def batched_nn_compute_grads(hypo_params, template_params, xs, zs):
    def nn_template(z):
        return template_apply(template_params, z).squeeze()
    def nn_compute_sdf_final(hypo_params, x):
        return nn_compute_vars(hypo_params, template_params, x)[-1]
    grad_deform = jax.vmap(jax.jacfwd(deform_apply, argnums=1))(hypo_params, xs)
    grad_temp = jax.vmap(jax.jacfwd(nn_template))(zs)
    grad_sdf = jax.vmap(jax.jacfwd(nn_compute_sdf_final, argnums=1))(hypo_params, xs)

    return grad_deform, grad_temp, grad_sdf

def loss_dif(hyper_params, template_params, idxs, xs, ns, ys):
    embeddings, hypo_params, zs, _, corrections, _, sdfs_final = batched_nn_compute_all(hyper_params, template_params, idxs, xs)
    grad_deform, grad_temp, grad_sdf = batched_nn_compute_grads(hypo_params, template_params, xs, zs)

    alpha = 100

    ys_off = (ys == -1) + 0
    ys_noff = 1-ys_off
    ys_on = (ys == 0) + 0
    ys_non = 1-ys_on

    sdf_term = (ys_noff*jnp.abs(clamp(sdfs_final, -0.5, 0.5)-clamp(ys, -0.5, 0.5))).mean()
    inter_term = (ys_off*jnp.exp(-alpha * jnp.abs(sdfs_final))).mean()
    normal_term = (ys_on*(1-cos_similarity(grad_sdf, ns))).mean()
    eikonen_term = jnp.abs(jnp.linalg.norm(grad_sdf, axis=1)-1).mean()

    embedding_term = (embeddings**2).mean()
    deform_term = jnp.linalg.norm(grad_deform, axis=-1).mean()
    template_term = (ys_on*(1-cos_similarity(grad_temp, ns))).mean() 
    correct_term = jnp.abs(corrections).mean()
    
    
    losses = jnp.array([sdf_term,inter_term,normal_term,eikonen_term,embedding_term,deform_term,template_term,correct_term])
    coefs = jnp.array([3e3, 5e2, 1e2, 5e1, 1e6, 5, 1e2, 1e2])
    return jnp.dot(losses, coefs), losses

# Optimizer function
@jax.jit
def update(step, hyper_opt_state, template_opt_state, idxs, xs, ns, ys):
    hyper_params, template_params = hyper_opt_params(hyper_opt_state), template_opt_params(template_opt_state)

    value, values = loss_dif(hyper_params, template_params, idxs, xs, ns, ys)

    def loss_dif_hyper(hyper_params, idxs, xs, ns, ys):
        return loss_dif(hyper_params, template_params, idxs, xs, ns, ys)[0]
    def loss_dif_template(template_params, idxs, xs, ns, ys):
        return loss_dif(hyper_params, template_params, idxs, xs, ns, ys)[0]

    grads_hyper = jax.grad(loss_dif_hyper)(hyper_params, idxs, xs, ns, ys)
    grads_temp = jax.grad(loss_dif_template)(template_params, idxs, xs, ns, ys)
    hyper_opt_state = hyper_opt_update(step, grads_hyper, hyper_opt_state)
    template_opt_state = template_opt_update(step, grads_temp, template_opt_state)
    return value, hyper_opt_state, template_opt_state, values

# Run training
total_steps = 0
checkpoint_epoch = num_epochs // 5
logpoint_epoch = num_epochs // 50
losses = []

# Generate samples
print(f'modelsDirectory: {modelsDirectory}')
modelNames = [name for name in os.listdir(modelsDirectory) if os.path.isdir(f'{modelsDirectory}/{name}')]
print(f'Found {len(modelNames)} shapes')
modelNames, n_kpc = read_models_with_keypoints(annotationsFile)
random.shuffle(modelNames)
modelNames = [modelNames[i] for i in range(n_models)]
print(f'Selected {n_models} models: {modelNames}')

modelSamples = []
models = []
mesh_labels = []
mesh_vertices = []

mesh_ids = {}
for i,name in enumerate(modelNames):
    baseFile = f'{modelsDirectory}/{name}/models/model_normalized'
    mesh = trimesh.load_mesh(f'{baseFile}.obj')
    mesh_ids[name] = i
    if isinstance(mesh, trimesh.Scene):
        meshes = mesh.dump(concatenate=False)
        mesh = trimesh.util.concatenate(meshes)
        mesh_labels.append(np.hstack([i*np.ones(len(mesh.vertices)) for i, mesh in enumerate(meshes)]))
    else:
        mesh_labels.append(np.zeros(len(mesh.vertices)))
    models.append(mesh)
    mesh_vertices.append(mesh.vertices)
    samples, n_samples2 = load_samples(baseFile, n_samples, force=False)
    print(f'Loaded {n_samples2} samples for model i={i} name={name}')
    modelSamples.append(samples)
    losses.append([])

mesh_kps = read_keypoints(annotationsFile, mesh_ids, n_kpc)

start_time = time.time()

if not flagTrain:
    num_epochs = 0

for epoch in range(num_epochs):
    for idx_model,samples in enumerate(modelSamples):
        # gt of size (1, sample_size, 7)
        X, S, N = samples[:,:3], samples[:,3], samples[:,4:]
        # X shape (sample_size, 3), S shape (sample_size)
        X, S, N = jnp.array(X), jnp.array(S), jnp.array(N)

        loss, hyper_opt_state, template_opt_state, values = update(epoch, hyper_opt_state, template_opt_state, idx_model*jnp.ones_like(S, dtype=int), X, N, S)
        losses[idx_model].append(values)
        total_steps += 1

    if (epoch + 1) % logpoint_epoch == 0:
        print(f'Epoch={epoch+1}, loss={loss} elapsed={time.time()-start_time}')

        start_time = time.time()

    if (epoch+1) % checkpoint_epoch == 0:
        if flagSave:
            pickle.dump((hyper_opt_params(hyper_opt_state), template_opt_params(template_opt_state)), open(f'{checkpointFile}.{epoch+1}', "wb"))
            print(f'Saved model params to {checkpointFile}.{epoch+1}')

        scene_elements = []
        origin_x = np.array([0, 2, 0])
        for idx_model, samples in enumerate(modelSamples):
            try:
                hyper_params, template_params = hyper_opt_params(hyper_opt_state), template_opt_params(template_opt_state)
                idxs = idx_model*jnp.ones((1,), dtype=int)
                embedding = latent_codes[idxs,:]
                hypo_params = hyper_apply(hyper_params, embedding)
                def nn_apply(x):
                    return nn_compute_vars(hypo_params, template_params, x)[-1]
                batched_nn_apply = jax.vmap(nn_apply)
                batched_nn_grads = jax.vmap(jax.jacfwd(nn_apply))
                
                origin = idx_model * np.array([2,0,0])
                points, gt_sdfs, normals = samples[:,:3], samples[:,3], samples[:,4:]
                grads_sdf = batched_nn_grads(points).squeeze()
                est_sdfs = batched_nn_apply(points).squeeze()
                
                pc_gt = trimesh_colored_pc(points, gt_sdfs, origin=origin-2*origin_x)
                pc_est = trimesh_colored_pc(points, est_sdfs, origin=origin-origin_x)
                mesh = trimesh.Trimesh(vertices=2*models[idx_model].vertices+origin+origin_x, faces=models[idx_model].faces)
                mesh2 = generateMeshFromSDF(batched_nn_apply, N = 256, origin=origin)
                pc_est_norms = trimesh_colored_normals(points, grads_sdf, origin=origin+2*origin_x)
                pc_gt_norms = trimesh_colored_normals(points, normals, origin=origin+3*origin_x)
                
                scene_elements.append([pc_gt,pc_est,mesh, mesh2, pc_est_norms, pc_gt_norms])
                
                # Avoid mesh from SDF generation crashes
            except RuntimeError:
                print('Runtime error: skipping')
                pass
        scene_elements = [item for sublist in scene_elements for item in sublist]
        def batched_template_apply(x):
            return template_apply(template_params, x)
        origin = -np.array([2,0,0])
        samples = np.random.uniform(-1,1,(2000,3))
        scene_elements.append(trimesh_colored_pc(samples, batched_template_apply(samples), origin=origin-origin_x))
        scene_elements.append(generateMeshFromSDF(batched_template_apply, N = 256, origin=origin))
        scene = trimesh.Scene(scene_elements)
        if flagVisualize:
            scene.show()
        scene.export(f'{visualizationDirectory}/dif.{epoch+1}.glb')
        
# Print losses graphs
if flagTrain:
    labels = ['sdf', 'inter', 'normal', 'eikonal', 'embedding', 'deform', 'template', 'correction']
    for idx_model in range(n_models):
        losses_arr = jnp.array(losses[idx_model]).T
        for k, loss in enumerate(losses_arr[:,100:]):
            plt.plot(range(len(loss)), loss, label=f'Model {idx_model}: {labels[k]}')
            plt.legend()
            plt.show()

    # Save model
    if flagSave:
        pickle.dump((hyper_opt_params(hyper_opt_state), template_opt_params(template_opt_state)), open(checkpointFile, "wb"))
        print(f'Saved model params to {checkpointFile}')

# Correspondance
hyper_params, template_params = hyper_opt_params(hyper_opt_state), template_opt_params(template_opt_state)
def nn_compute_template_coords(hypo_params, x):
    return nn_compute_vars(hypo_params, template_params, x)[0]
batched_nn_compute_template_coords = jax.vmap(nn_compute_template_coords, in_axes=(None,0))

# Compute template coords
tcoords = []
kdts = []
meshes = []
for i in range(n_models):
    verts = mesh_kps[i]#mesh_vertices[i]
    idxs = i*jnp.ones((1,), dtype=int)
    embedding = latent_codes[idxs,:]
    hypo_params = hyper_apply(hyper_params, embedding)
    tcoords.append(batched_nn_compute_template_coords(hypo_params, verts))
    print(tcoords[i].shape)
    tcoords[i] = verts
    kdts.append(KDTree(tcoords[i]))
    meshes.append(trimesh_colored_pc(verts, mesh_labels[i], origin=np.array([0, 0,i]), method='scaled'))
    meshes.append(trimesh_colored_pc(tcoords[i], mesh_labels[i], origin=np.array([0, -1,i]), method='scaled'))

scene = trimesh.Scene(meshes)
#scene.show()
# Compute correspondance
# Transfer labels of i to j
# Transform mesh i to mesh j, use knn to get labels of vertices of mesh j from mesh i labels
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix
from scipy.stats import mode

meshes = []
k = 3
#gt, pred, pred_nn = [], [], []
correct = np.zeros((n_models, n_models), dtype=np.int32)
i_label = 0
for i in range(n_models):
    for j in range(n_models):
       nn = kdts[i].query(tcoords[j], k=1, return_distance=False)
       #nn = mode(kdts[i].query(tcoords[j], k=k, return_distance=False).T)[0][0]
       #transferred_labels = mesh_labels[i][nn]
       transferred_labels = nn.squeeze()#mesh_labels[i][nn]
       #pred_labels_nn = mode(KDTree(mesh_kps[i], leaf_size=30).query(mesh_kps[j], k=k, return_distance=False).T)[0][0]
       #pred_labels_nn = KDTree(mesh_kps[i], leaf_size=30).query(mesh_kps[j], k=k, return_distance=False)
       pred_labels_nn = KDTree(mesh_kps[i]).query(mesh_kps[j], k=1, return_distance=False).squeeze()
       print(f'preds: {pred_labels_nn}')
       print(f'trans: {transferred_labels}')

       correct[i,j] = (transferred_labels == pred_labels_nn).sum()
       #pred_labels_nn = mesh_labels[i][pred_labels_nn]
       #gt_labels = mesh_labels[j] 
       #gt.append(i_label + gt_labels)
       #pred.append(i_label + transferred_labels)
       #pred_nn.append(i_label + pred_labels_nn)
       #i_label += np.max(gt_labels)+1
       acc = correct[i,j] / len(transferred_labels)
       print(f"Transfering labels of i={i} to j={j}: acc={acc*100:.2f} %%")
       #print(confusion_matrix(gt_labels, transferred_labels))
       meshes.append(trimesh_colored_pc(mesh_kps[j], transferred_labels, origin=np.array([j, i,0]), method='scaled', m = 0, M = n_kpc-1))
       meshes.append(trimesh_colored_pc(mesh_kps[j], pred_labels_nn, origin=np.array([j, i, 0.01]), method='scaled', m = 0, M = n_kpc-1))

# Print statistics
if False:
    gt = np.hstack(gt)
    pred = np.hstack(pred)
    pred_nn = np.hstack(pred_nn)
    print(classification_report(gt, pred))
    print(classification_report(gt, pred_nn))

# Visualization 
scene = trimesh.Scene(meshes)
scene.show()
