# 2022-01-22

n_samples = 2**13#2 ** 15
n_models = 10 #100
learning_rate = 1e-4
num_epochs = 1000#100000
checkpoint_epoch = 100 # num_epochs // 5
logpoint_epoch = 10# num_epochs // 50

# no need for more than 3 x 64
hidf = 64 # 128
n_hidden = 3 # 1

# latent code dimension
latent_dim = 32 # 128

# difnet uses these parameters: quite sensible (waves, also impacts template)
hhidf = 128 # 256
hn_hidden = 1
