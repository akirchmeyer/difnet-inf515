import numpy as np
import jax
import jax.numpy as jnp
from jax.experimental import optimizers, stax

# Utilities
def get_shape(x):
    return x.shape if hasattr(x, 'shape') else x#type(x)
def net_shape(net):
    return jax.tree_map(lambda x : get_shape(x), net)

# Instantiate net
def nn_instance(rng, inf, n_samples, net):
    nn_init, nn_apply = net
    output_shape, params = nn_init(rng, (n_samples,inf))
    return params, nn_apply

# ReLU ativation
def nl_relu(xs):
    return jax.nn.relu(xs) 
def relu_init_func(first=False, stddev=0.1):
    #return jax.nn.initializers.kaiming_normal()
    return jax.nn.initializers.normal(stddev=stddev)

# Sine activation
def nl_sine(xs):
    return jnp.sin(30*xs)

def symmetric_uniform(scale=1e-2, mode='fan_in', dtype=jnp.float_, in_axis=-2, out_axis=-1, batch_axis=()):
  def init(key, shape, dtype=dtype):
    dtype = jax.dtypes.canonicalize_dtype(dtype)
    shape = jax.core.as_named_shape(shape)
    if mode == "fan_in": denominator = shape[in_axis]
    else: denominator=1
    return jax.random.uniform(key, shape, dtype, minval=-scale/denominator, maxval=scale/denominator)
  return init

def sine_init_func(first=False):
    if first:
        return symmetric_uniform(scale=1, mode="fan_in")
    #alpha = jnp.sqrt(6/inf) / 30
    #return symmetric_uniform(scale=alpha)
    return partial(jax.nn.initializers.variance_scaling)(scale=2/900, mode="fan_in", distribution="uniform")

# BVP net
def nn_bvp(inf, hidf, outf, n_hidden, type='sine'):
    if type == 'sine':
        nl, nl_init = stax.elementwise(nl_sine), sine_init_func
    elif type == 'relu':
        nl, nl_init = stax.elementwise(nl_relu), relu_init_func
    elif type == 'hyper':
        stddev = 0.1 / 1e2
        nl, nl_init = stax.elementwise(nl_relu), partial(relu_init_func, stddev=stddev)
    else:
        nl, nl_init = type

    nets = [stax.Dense(hidf, W_init=nl_init(first=True)), nl]
    for i in range(n_hidden-1):
        nets.append(stax.Dense(hidf, W_init=nl_init()))
        nets.append(nl)
    nets.append(stax.Dense(outf, W_init=nl_init()))
    return stax.serial(*nets)

from functools import partial

def nn_hyper(inf, hidf, n_hidden, params):
    flattened, treedef = jax.tree_flatten(params)
    sizes = [x.size for x in flattened]
    shapes = [x.shape for x in flattened]

    nets = [nn_bvp(inf, hidf, size, n_hidden, type='hyper') for size in sizes]
    nn_init, nn_apply = parallel2(*nets)
    def nn_apply_unflattened(params, inputs):
        n_samples = inputs.shape[0]
        outputs = nn_apply(params, inputs)
        outputs = [output.reshape((n_samples,)+shape) for output,shape in zip(outputs, shapes)]
        return jax.tree_unflatten(treedef, outputs)
    return nn_init, nn_apply_unflattened

# Same as stax.parallel but supposes every module uses same input
def parallel2(*layers):
  nlayers = len(layers)
  init_funs, apply_funs = [layer[0] for layer in layers], [layer[1] for layer in layers]
  def init_fun(rng, input_shape):
    rngs = jax.random.split(rng, nlayers)
    params = [init(rngs[i], input_shape) for i, init in enumerate(init_funs)]
    output_shape = [param[0] for param in params] # not tested
    params = [param[1] for param in params]
    return output_shape, params
  def apply_fun(params, inputs):
      outputs = [apply(params[i], inputs) for i, apply in enumerate(apply_funs)]
      return outputs

  return init_fun, apply_fun

def clamp(x, a, b):
    return jnp.maximum(jnp.minimum(x, b), a)

def gaussian_fourier(B):
    def init_fun(rng, input_shape):
        m,d = B.shape
        output_shape = (2*m,)
        params = []
        return output_shape, params
    def apply_fun(params, inputs, **kwargs):
        a = jnp.dot(B, inputs.T).T
        ca = jnp.cos(2*jnp.pi*a)
        sa = jnp.sin(2*jnp.pi*a)
        outputs = jnp.hstack([ca, sa])
        return outputs
    return init_fun, apply_fun


def nn_bvp_fourier(inf, hidf, outf, n_hidden, B, type='relu'):
    if type == 'sine':
        nl, nl_init = stax.elementwise(nl_sine), sine_init_func
    else:
        nl, nl_init = stax.elementwise(nl_relu), relu_init_func
    inf2 = 2*B.shape[0]
    nets = [gaussian_fourier(B), stax.Dense(hidf, W_init=nl_init(inf2, first=True)), nl]
    for i in range(n_hidden-1):
        nets.append(stax.Dense(hidf, W_init=nl_init(hidf)))
        nets.append(nl)
    nets.append(stax.Dense(outf, W_init=nl_init(hidf)))
    return stax.serial(*nets)
