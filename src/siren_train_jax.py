# Modules
import time, os
import numpy as np
import jax
import jax.numpy as jnp
import pickle
import matplotlib.pyplot as plt
import trimesh

from utils import *
from models import *
from config import baseDirectory, modelDirectory, visualizationDirectory

# Colored output on terminal
import sys
from IPython.core import ultratb
sys.excepthook = ultratb.FormattedTB( color_scheme='Linux', call_pdb=False)

# Paths
baseFile = f'{modelDirectory}/models/model_normalized'
checkpointFile = f'{baseDirectory}/checkpoints/best.siren.ckpt.pkl'

# Constants
n_samples = 2 ** 16
learning_rate = 1e-4
num_epochs = 5000
checkpoint_epoch = 500
logpoint_epoch = 100
hidf = 128#128
n_hidden = 3#3
#n_hidden = 6
seed = 0


flagVisualize = True
flagLoad = False
flagSave = False
flagUseFourierFeatures = False

# Generate samples
samples, n_samples = load_samples(baseFile, n_samples, force = False)
print(np.min(samples[:,3]), np.max(samples[:,3]))
pc_gt = trimesh_colored_pc(samples[:,:3], samples[:,3], method='posneg')
#trimesh.Scene([pc_gt]).show()

# Create network
key = jax.random.PRNGKey(seed)
key, key1, key2 = jax.random.split(key, 3)


params, nn_apply = nn_instance(key1, inf=3, n_samples=n_samples, net=nn_bvp(inf=3, hidf=hidf, outf=1, n_hidden=n_hidden, type='sine'))

if flagUseFourierFeatures:
    sigma = 30 
    m = 32 #4
    B = sigma*jax.random.normal(key2, shape=(m,3))
    B = np.zeros((3*m,3))
    for i in range(m):
        B[3*i:3*i+3, :] = ((2**i)/(2*np.pi)) * np.eye(3)

    params, nn_apply = nn_instance(key1, inf=3, n_samples=n_samples, net=nn_bvp_fourier(inf=3, hidf=hidf, outf=1, n_hidden=n_hidden, B=B, type='sine'))


print('Network shape:', jax.tree_map(lambda x: x.shape if hasattr(x, 'shape') else type(x), params))

# Load model
if flagLoad:
    params = pickle.load(open(checkpointFile, "rb"))
    print(f'Loaded model params from {checkpointFile}')

(opt_init, opt_update, opt_params) = optimizers.adam(step_size=learning_rate)
opt_state = opt_init(params)

# Loss function
def loss_sdf(params, xs, ns, ys):
    grads_sdf = jax.vmap(jax.jacfwd(nn_apply, argnums=1), in_axes=(None,0))(params, xs).squeeze()
    sdfs = nn_apply(params, xs).squeeze()

    sdf_term = jnp.abs(clamp(sdfs,-0.5,0.5)-clamp(ys,-0.5,0.5)).mean()
    #sdf_term = jnp.abs(sdfs-ys).mean()
    inter_term = 0
    normal_term = (1-cos_similarity(grads_sdf, ns)).mean()
    eikonen_term = jnp.abs(jnp.linalg.norm(grads_sdf, axis=1)-1).mean()
    return sdf_term,inter_term,normal_term,eikonen_term

def loss_sdf0(params, xs, ns, ys):
    grads_sdf = jax.vmap(jax.jacfwd(nn_apply, argnums=1), in_axes=(None,0))(params, xs).squeeze()
    sdfs = nn_apply(params, xs).squeeze()
    alpha = 100
    ys_off = (ys == -1) + 0
    ys_noff = 1-ys_off
    ys_on = (ys == 0) + 0
    ys_non = 1-ys_on

    sdf_term = jnp.abs(ys_noff*(clamp(sdfs,-0.5,0.5)-clamp(ys,-0.5,0.5))).mean()
    #sdf_term = jnp.abs(sdfs-ys).mean()
    inter_term = (ys_off*(jnp.exp(-alpha * jnp.abs(sdfs)))).mean()
    normal_term = (ys_non*(1-cos_similarity(grads_sdf, ns))).mean()
    eikonen_term = jnp.abs(jnp.linalg.norm(grads_sdf, axis=1)-1).mean()
    return sdf_term,inter_term,normal_term,eikonen_term

def loss_sdf_summed(params, x, n, y):
    losses = jnp.array(loss_sdf(params, x, n, y))
    coefs = jnp.array([3e3, 1e2, 1e2, 5e1])
    return jnp.dot(losses, coefs)
    
# Optimizer function
@jax.jit
def update(step, opt_state, xs, ns, ys):
  value, grads = jax.value_and_grad(loss_sdf_summed)(opt_params(opt_state), xs, ns, ys)
  values = loss_sdf(opt_params(opt_state), xs, ns, ys)
  #opt_state = opt_update(step, jax.tree_map(lambda x : jnp.clip(x, -1, 1), grads), opt_state)
  opt_state = opt_update(step, grads, opt_state)
  return value, opt_state, values

# Run training
total_steps = 0
losses = []
start_time = time.time()

# gt of size (1, sample_size, 7)
X, S, N = samples[:,:3], samples[:,3], samples[:,4:]
# X shape (sample_size, 3), S shape (sample_size)
X, S, N = jnp.array(X), jnp.array(S), jnp.array(N)

for epoch in range(num_epochs):
    loss, opt_state, values = update(epoch, opt_state, X, N, S)
    losses.append(values)
    total_steps += 1

    if (epoch + 1) % logpoint_epoch == 0:
        print(f'Epoch={epoch+1}, loss={loss} elapsed={time.time()-start_time}')
        start_time = time.time()

    # Visualization
    if (epoch+1) % checkpoint_epoch == 0:
        try:
            def nn_apply2(x):
                return nn_apply(opt_params(opt_state), x)
            origin_x, origin_y = np.array([2, 0, 0]), np.array([0, 2, 0])

            points, gt_sdfs, normals = samples[:,:3], samples[:,3], samples[:,4:]
            grads_sdf = jax.vmap(jax.jacfwd(nn_apply2))(points).squeeze()
            est_sdfs = nn_apply2(points).squeeze()

            pc_gt = trimesh_colored_pc(points, gt_sdfs, origin=-origin_x)
            pc_gt_norms = trimesh_colored_normals(points, normals, origin=-origin_x-origin_y)
            pc_est = trimesh_colored_pc(points, est_sdfs, origin=origin_x)
            pc_est_norms = trimesh_colored_normals(points, grads_sdf, origin=origin_x-origin_y)
            mesh2 = generateMeshFromSDF(nn_apply2, N = 256, origin = np.array([0,0,0]))

            #scene = trimesh.Scene([pc_gt, pc_gt_norms, mesh2, pc_est, pc_est_norms])
            scene = trimesh.Scene([mesh2])
            scene.export(f'{visualizationDirectory}/siren.{epoch+1}.glb')
            if flagVisualize:
                scene.show()

        # Avoid mesh from SDF generation crashes
        except RuntimeError:
            print('Runtime error: skipping')
            pass

# Print losses graphs
labels = ['sdf', 'inter', 'normal', 'eikonal', 'embedding', 'deform', 'template', 'correction']
losses = jnp.array(losses).T
for k, loss in enumerate(losses[:,100:]):
    plt.plot(range(len(loss)), loss, label=labels[k])
plt.legend()
plt.show()

# Save model
if flagSave:
    pickle.dump(opt_params(opt_state), open(checkpointFile, "wb"))
    print(f'Saved model params to {checkpointFile}')


# Train yellow motorcycle: complex test case 
# - loss0 / loss
# - SIREN reconstruction quality
# - shows this strategy only works with good sampling -> scan / sample, depth / normal
# --> sampling requires ~N^2 for same details fidelity (can become N^3 if complex eg motor or pipes)
# - bubbles for normal method

# Encode compositionality: scene
# - Add subcomponents into training set
# - Add parts to training set

# Sampling has difficulty encoding high frequency details

# SIREN Gaussian sampling: 2**17, 38s/100epochs m=128 h=256*3 sigma=0.3
# convergence 1000: 55[500] -> 40[1000] -> 35[1500]

# convergence: 32s/100epochs h=256*3 45[500] -> 
# subtlety between gaussian features + SIREN: differne tinitialization
# --> additional layers encode higher frequencies 
