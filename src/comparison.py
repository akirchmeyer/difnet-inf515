import pymeshlab
import numpy as np

from os import listdir
from os.path import isfile, join, isdir
from sklearn.neighbors import KDTree
import matplotlib.pyplot as plt
import random

from config import baseDirectory

import json
import sys


#threshold_list = [0.01 * i for i in range(11)]
threshold_list = np.linspace(0, 0.5, num=11, endpoint=True)
def eval_pck(errors, th = threshold_list):
    errors = np.array(errors)
    return np.array([(errors <= x).mean() for x in th])

def read_keypoints(annotationFile, model_ids, n_kpc):
    data = json.load(open(annotationFile, 'rb'))
    res = np.zeros((len(model_ids), n_kpc, 3))
    for mesh_keypoint in data:
        model_id = mesh_keypoint['model_id']
        if not model_id in model_ids:
            continue
        res_id = model_ids[model_id]
        for model_kp in mesh_keypoint['keypoints']:
            res[res_id, model_kp['semantic_id'], :] = np.array(model_kp['xyz'])
    return res

n_tests = 1000
n_models = 200

dataset = sys.argv[1] if len(sys.argv) >= 2 else 'chair'

if dataset == 'chair':
    path = f'{baseDirectory}/recon/chair'
    annotation = f'{baseDirectory}/datasets/annotations/chair.json'
    dataset_path = f'{baseDirectory}/datasets/03001627'

if dataset == 'plane':
    path = f'{baseDirectory}/recon/plane'
    annotation = f'{baseDirectory}/datasets/annotations/airplane.json'
    dataset_path = f'{baseDirectory}/datasets/02691156'

if dataset == 'table':
    path = f'{baseDirectory}/recon/table'
    annotation = f'{baseDirectory}/datasets/annotations/table.json'
    dataset_path = f'{baseDirectory}/datasets/04379243'

if dataset == 'car':
    path = f'{baseDirectory}/recon/car'
    annotation = f'{baseDirectory}/datasets/annotations/car.json'
    dataset_path = f'{baseDirectory}/datasets/02958343'

model_ids = {}
dataset_filenames = [f for f in listdir(dataset_path) if isdir(join(dataset_path, f))]
for i,filename in enumerate(dataset_filenames):
    if i == n_models:
        break
    model_ids[filename] = i
print('Indexed models')

n_kpc = 22 # manually found
kps = read_keypoints(annotation, model_ids, n_kpc)
print('Read keypoints')
    
verts, colors = [], []
kdts = []
vkdts = []
ckdts = []
errors = []
errors_nn = []

for k in range(n_models):
    ms = pymeshlab.MeshSet()
    ms.load_new_mesh(f'{path}/test{k:04}.ply')
    verts.append(ms.current_mesh().vertex_matrix())
    colors.append(ms.current_mesh().vertex_color_matrix()[:,:3])
    kdts.append(KDTree(verts[k]))
    ckdts.append(KDTree(colors[k]))

    #ms.load_new_mesh(f'{dataset_path}/{dataset_filenames[k]}/models/model_normalized.obj')
    #vkdts.append(KDTree(kps[k,:,:]))#ms.current_mesh().vertex_matrix()))
    print(f'k={k}')

print('Read meshes')
    

errors_models = []
errors_nn_models = []
for i in range(n_models):
    for j in range(n_models):
        errors_model = []
        errors_nn_model = []
        kpi = kps[i,:,:]
        kpj = kps[j,:,:]

        xi = kdts[i].query(kpi, return_distance=False).squeeze()
        ci = colors[i][xi]
        ti = verts[j][ckdts[j].query(ci, return_distance=False).squeeze()]
        #xj = kdts[j].query(kpj, return_distance=False).squeeze()
        #cj = colors[j][xj]
        #tj = verts[i][ckdts[i].query(cj, return_distance=False).squeeze()]

        nn_ti = verts[j][kdts[j].query(kpi, return_distance=False).squeeze()] # for every l, kpi[l,:]
        #nn_tj = vkdts[i].query(kpj, return_distance=False).squeeze() # for every l, kpj[l,:]
        
        for l in range(n_kpc):
            #if (kpi[l,:] != 0).sum() > 0 and (kpj[l,:] != 0).sum() > 0:
            errors_model.append(np.linalg.norm(kpj[l,:]-ti[l]))
            #errors.append(np.linalg.norm(kpi[l,:]-tj[l]))
            errors_nn_model.append(np.linalg.norm(kpj[l,:]-nn_ti[l]))
            #errors_nn.append(np.linalg.norm(kpi[l,:]-nn_tj[l]))
            #if (k+1) % 1000 == 0:
            #    print(f'i={i} j={j} mean {k}/{n_tests}: difnet={np.mean(errors)} nn={np.mean(errors_nn)}')
            #    print(f'i={i} j={j} pck  {k}/{n_tests}: difnet={100*(np.array(errors)<=0.01).mean()}% nn={100*(np.array(errors_nn)<=0.01).mean()}%')

            #print(f'i={t} dif={eval_pck(errors_model)} nn={eval_pck(errors_nn_model)}')
            errors_models += errors_model
            errors_nn_models += errors_nn_model

print(np.array(errors_models).shape, np.array(errors_nn_models).shape)
pck_models = eval_pck(errors_models)
pck_nn_models = eval_pck(errors_nn_models)

print(f'[{dataset}]')
print('pck dif:', np.round(pck_models, 3))
print('pck nn:', np.round(pck_nn_models, 3))

A = np.linspace(0, 0.5, num=1000, endpoint=True)
plt.xticks(np.linspace(0,0.5,num=10,endpoint=True))
plt.yticks(np.linspace(0,1,num=10,endpoint=True))
plt.grid()
plt.plot(A, eval_pck(errors_models, A), label='DIF-Net')
plt.plot(A, eval_pck(errors_nn_models, A), label='Nearest')
plt.title(dataset)
plt.legend()
plt.show()
