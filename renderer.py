
import trimesh
import time
import numpy as np
n_meshes = 25
baseDirectory = '/opt/X3_2022/INF515'
interp_meshes = [trimesh.load_mesh(f'{baseDirectory}/output2/interpolation2-{idx}.ply') for idx in range(n_meshes)]

i = 0
def interpolation_realtime(scene):
    global i 
    j = int((time.time())%n_meshes)
    if i == j:
        return
    i = j
    scene.delete_geometry(["sdf"])
    scene.add_geometry(interp_meshes[i], geom_name="sdf")

scene = trimesh.Scene([trimesh.PointCloud(vertices=interp_meshes[0].bounds)])
scene.show(callback = interpolation_realtime)
